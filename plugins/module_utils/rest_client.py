from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

import json

from ansible_collections.spbstu.swordfish.plugins.module_utils.client.exceptions import RESTClientUnauthorized, \
    RESTClientRequestError, RESTClientConnectionError
from ansible_collections.spbstu.swordfish.plugins.module_utils.client.http import HTTPClient
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.fabrics.fabric import Fabric

try:
    from typing import Optional, List
except ImportError:
    # Satisfy Python 2 which doesn't have typing.
    Optional = List = None

from ansible.module_utils.six.moves.urllib.error import HTTPError
from ansible_collections.spbstu.swordfish.plugins.module_utils.exception import (
    RESTClientNotFoundError,
)
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.drive_group import DriveGroup
from ansible_collections.spbstu.swordfish.plugins.module_utils.endpoints import (
    STORAGE_ENDPOINT,
    ROOT_ENDPOINT,
)
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.manager.manager import Manager
from ansible_collections.spbstu.swordfish.plugins.module_utils.models.storage.storage import Storage
from ansible.module_utils.six.moves.urllib.error import URLError, HTTPError
from ansible.module_utils.urls import ConnectionError, SSLValidationError


class RESTClient(HTTPClient):
    def __init__(
            self,
            *args,
            **kwargs
    ):
        super(RESTClient, self).__init__(*args, **kwargs)

    def make_request(self, path, method, query_params=None, body=None, headers=None):
        # type: (str, str, Dict, Dict, Dict) -> HTTPClientResponse
        try:
            response = self._make_request(path, method, query_params, body, headers)
        except HTTPError as e:
            if e.code == 404:
                raise RESTClientNotFoundError("Not found: {0}".format(e.url))
            elif e.code == 401:
                raise RESTClientUnauthorized('Unauthorized error')
            else:
                try:
                    msg = json.load(e)
                except ValueError:
                    msg = str(e)
                raise RESTClientRequestError("Request finished with error: {0}".format(msg))
        except (URLError, SSLValidationError, ConnectionError) as e:
            raise RESTClientConnectionError("Cannot connect to server: {0}".format(str(e)))
        else:
            return response
