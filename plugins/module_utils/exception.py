from __future__ import (absolute_import, division, print_function)

__metaclass__ = type


class SwordfishError(Exception):
    pass


class SwordfishAPIError(SwordfishError):
    pass


class SwordfishFieldNotFoundError(SwordfishAPIError):

    def __init__(self, name):  # type: (str) -> None
        self.name = name

    def __str__(self):  # type: () -> str
        return "Field not found: {0}".format(self.name)


class SwordfishModelLoadError(SwordfishAPIError):
    pass


class SwordfishModelVersionError(SwordfishAPIError):

    def __init__(self, version):  # type: (str) -> None
        self.version = version

    def __str__(self):  # type: () -> str
        return "Swordfish object unsupported: {0}".format(self.version)


class RESTClientError(SwordfishError):
    pass


class RESTAuthorizationError(SwordfishError):
    pass


class RESTClientNotFoundError(SwordfishError):
    pass
